srctype = micropython-lib
type = package
version = 2.2
depends = copy, types, pathlib-full
author = Andrew Leech
author_email = andrew@alelec.net